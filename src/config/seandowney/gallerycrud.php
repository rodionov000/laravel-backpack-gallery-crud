<?php

return [

    /*
    | SeanDowney/GalleryCrud configs.
    */

    /**
     * Path used in the routes for the glide images
     */
    'glide_path' => 'img',

];
